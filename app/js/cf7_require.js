$(document).ready(function(){
	jQuery(".wpcf7-form-control").prop('required',true);
	jQuery(".wpcf7-form").removeAttr('novalidate');
});


$(document).ready(function() {
    document.addEventListener( 'wpcf7mailsent', function( event ) {
        $('.get-price-popup').hide(350);
        $('.wpcf7-response-output, wpcf7-mail-sent-ok').remove();
        $('.sent-form-popup').show(350);
    }, false );
});
