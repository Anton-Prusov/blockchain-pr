$(document).ready(function(){

    // Header fade in

    setTimeout(function () {
        $('.topline').css({
            transition: 'all .8s',
            transform: 'translateY(0)',
            opacity: '1'
        });
    }, 500);

    setTimeout(function () {
        $('.one-bg').css({
            transition: 'all .8s',
            transform: 'translate(-50%, -50%)',
            opacity: '1'
        });
        $('.header-main h1, .header-main h3, .header-main p').css({
            transition: 'all .8s',
            transform: 'translateY(0)',
            opacity: '1'
        });
    }, 1000);

    setTimeout(function () {
        $('.header-main a').css({
            transition: 'all .8s',
            transform: 'translateY(0)',
            opacity: '1'
        });
    }, 1500);

    // SVG hovers

    $('.social-icon').hover(
        function () {
            $(this).find($('svg')).find($('path')).attr('fill', '#E2FF6C');
        },
        function () {
            $(this).find($('svg')).find($('path')).attr('fill', '#DFEAEA');
        }
    );

    $('.arrow' ).hover(
        function () {
            $(this).find($('svg')).find($('path')).attr('stroke', '#E2FF6C');
        },
        function () {
            $(this).find($('svg')).find($('path')).attr('stroke', 'white');
        }
    );

    // Service Tabs

    var $services = $('.service-info');
    var $serviceLinks = $('.services-list ul li a');
    var $serviceDots = $('.services-dots i');

    $($services).css('display', 'flex').hide();
    $($services[0]).show();
    $($serviceLinks[0]).addClass('activeLink');
    $($serviceDots[0]).addClass('activeDot');

    $serviceDots.each(function() {
        let $dot = $(this);

        $dot.on('click', function (e) {
            e.preventDefault();
            var index = $serviceDots.index( $dot );

            if(!$($dot).hasClass('activeDot')) {
                $serviceDots.removeClass('activeDot');
                $dot.addClass('activeDot');
                $serviceLinks.removeClass('activeLink');
                $($serviceLinks[index]).addClass('activeLink');
                $($services).hide(350);
                $($services[index]).show(350);
            }
        });
    });

    $serviceLinks.each(function() {
        let $link = $(this);

        $link.on('click', function (e) {
            e.preventDefault();
            var index = $serviceLinks.index( $link );

            if(!$($link).hasClass('activeLink')) {
                $serviceLinks.removeClass('activeLink');
                $link.addClass('activeLink');
                $($services).hide(350);
                $($services[index]).show(350);
                $serviceDots.removeClass('activeDot');
                $($serviceDots[index]).addClass('activeDot');
            }
        });
    });

    // Expert - slider
/*
    var $authors = $('.expert-author');
    var $authorsArr = [];
    $($authors).each(function () {
        $authorsArr.push( $(this) );
    });
    var $messages = $('.expert-message');

    var $messagesArr = [];
    $($messages).each(function () {
        $messagesArr.push( $(this) );
    });
    $($authors).hide();
    $($messages).hide();
    $($authorsArr[0]).show().addClass('active');
    $($messagesArr[0]).show().addClass('active');

    $('.expert-arrow-right').click(function () {
       for (var i = 0; i < $authorsArr.length; i++) {
           if ( $($authorsArr[i]).hasClass('active') ) {
               if(i === $authors.length - 1) {
                   $($authorsArr[i]).hide().removeClass('active');
                   $($messagesArr[i]).hide().removeClass('active');
                   $($authorsArr[0]).show().addClass('active');
                   $($messagesArr[0]).show().addClass('active');
               }

               $($authorsArr[i]).hide().removeClass('active');
               $($messagesArr[i]).hide().removeClass('active');
               $($authorsArr[i+1]).show().addClass('active');
               $($messagesArr[i+1]).show().addClass('active');
               break
           }
       }
    });

    $('.expert-arrow-left').click(function () {
        for (var i = 0; i < $authorsArr.length; i++) {
            if ( $($authorsArr[i]).hasClass('active') ) {
                if(i === 0) {
                    $($authorsArr[i]).hide().removeClass('active');
                    $($messagesArr[i]).hide().removeClass('active');
                    $($authorsArr[$authors.length - 1]).show().addClass('active');
                    $($messagesArr[$authors.length - 1]).show().addClass('active');
                }

                $($authorsArr[i]).hide().removeClass('active');
                $($messagesArr[i]).hide().removeClass('active');
                $($authorsArr[i-1]).show().addClass('active');
                $($messagesArr[i-1]).show().addClass('active');
                break
            };
        }
    })*/

    // Portfolio popup

    let $portfolioPopupTabs = $('.portfolio-popup-content');
    let $portfolioPopupTabsArr = [];
    $($portfolioPopupTabs).each(function () {
        $(this).hide();
        $portfolioPopupTabsArr.push( $(this) );
    });

    $('#portfolio').on('click', '.portfolio-item a', function (e) {
        e.preventDefault();

        let $portfolioPopupLinks = $('#portfolio .portfolio-item a');
        let $popupLink = $(this);
        let index = $portfolioPopupLinks.index( $popupLink );
        $portfolioPopupTabs = $('.portfolio-popup-content');
        $($portfolioPopupTabs).each(function () {
            $(this).hide();
            $portfolioPopupTabsArr.push( $(this) );
        });

        // Show popup and active tab
        $('#popup_wrapper, .portfolio-popup').show(350);
        $('.portfolio-popup').css('display', 'flex');
        $($portfolioPopupTabsArr[index]).show().addClass('activeTab');

        // Close popup by click on close button
        $('.portfolio-popup .close').click(function () {
            $('#popup_wrapper, .portfolio-popup').hide(350);
            $portfolioPopupTabsArr.forEach((item) => {
                $(item).hide().removeClass('activeTab');
            });
        });

        // Close popup by click on wrapper
        $('#popup_wrapper').click(function (e) {
            e.preventDefault();
            var $tgt = $(e.target);
            if ( !$tgt.closest(".portfolio-popup").length ) {
                $('#popup_wrapper, .portfolio-popup').hide(350);
                $portfolioPopupTabsArr.forEach((item) => {
                    $(item).hide().removeClass('activeTab');
                });
            }
        });
    });

    // Slide to next tab
    $('.portfolio-popup-arrow-right').click(function () {
        let wasFound = false;

        $portfolioPopupTabsArr.forEach((el, i, arr) => {
            if (wasFound) return;
            if ( el.hasClass('activeTab') ) {
                wasFound = true;

                if( i === arr.length-1 ) {
                    $(el).hide().removeClass('activeTab');
                    $(arr[0]).show(200).addClass('activeTab');
                }
                $(el).hide().removeClass('activeTab');
                $(arr[i+1]).show(200).addClass('activeTab');
            }
        });
    });

    // Slide to previous tab
    $('.portfolio-popup-arrow-left').click(function () {
        let wasFound = false;

        $portfolioPopupTabsArr.forEach((el, i, arr) => {
            if (wasFound) return;
            if ( el.hasClass('activeTab') ) {
                wasFound = true;
                if(i === 0) {
                    $(el).hide().removeClass('activeTab');
                    $(arr[arr.length-1]).show(200).addClass('activeTab');
                }
                $(el).hide().removeClass('activeTab');
                $(arr[i-1]).show(200).addClass('activeTab');
            }
        });
    });

    // Get-price popup

    $('#get-price, a.button-get-price').click(function (e) {
        e.preventDefault();

        $('#popup_wrapper, .get-price-popup').show(350);
        $('.portfolio-popup').hide(350);
        $portfolioPopupTabsArr.forEach((item) => {
            $(item).hide().removeClass('activeTab');
        });
    });

    $('.get-price-popup .close').click(function () {
        $('#popup_wrapper, .get-price-popup').hide(350);
        $('.get-price-popup input[type!="submit"]').val('');
        $('.get-price-popup input[type= "checkbox"]').prop( "checked", false );
    });

    $('#popup_wrapper').click(function (e) {
        e.preventDefault();
        var $tgt = $(e.target);
        if ( !$tgt.closest(".get-price-popup").length ) {
            $('#popup_wrapper, .get-price-popup').hide(350);
            $('.get-price-popup input[type!="submit"]').val('');
            $('.get-price-popup input[type= "checkbox"]').prop( "checked", false );
        }
    });

    /*$('.get-price-popup form').submit(function (e) {
        e.preventDefault();

        $('.get-price-popup').hide(350);
        $('.sent-form-popup').show(350);
        $('.get-price-popup input').val('');
        $('.get-price-popup input[type= "checkbox"]').prop( "checked", false );
    });*/

    // Sent form popup

    $('.sent-form-popup .close').click(function () {
        $('#popup_wrapper, .sent-form-popup').hide(350);
    });

    $('.sent-form-popup .sent-form-popup-bottom a').click(function (e) {
        e.preventDefault();

        $('#popup_wrapper, .sent-form-popup').hide(350);
    });

    $('#popup_wrapper').click(function (e) {
        e.preventDefault();
        var $tgt = $(e.target);
        if ( !$tgt.closest(".sent-form-popup").length ) {
            $('#popup_wrapper, .sent-form-popup').hide(350);
        }
    });

    // Ankors

    $(".footer-nav, .header-nav-left, .header-nav-right, .mob-nav").on("click", "a", function (e) {
        e.preventDefault();

        var id  = $(this).attr('href');
        var top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    });

    // Mobile menu

    $('.mob-menu-btn').mouseup(function () {
        if($(".mob-menu").hasClass('visible')) {
            $('.mob-menu').hide();
        }
        else {
            $('.mob-menu').show();
        }
        $('.mob-menu').toggleClass('visible');
    });

    $('body').mouseup(function (e) {
        e.preventDefault();
        var $tgt = $(e.target);
            if ( !$tgt.closest(".mob-menu, .mob-menu-btn").length ) {
                if($(".mob-menu").hasClass('visible')) {
                    $('.mob-menu').hide().removeClass('visible');
                }
            }
    });

    // Simplebar

    let $portfolioInfoText = $('.portfolio-item-info .small-text');
    $portfolioInfoText.each(function () {
        new SimpleBar($(this)[0]);
    });

    let $portfolioDesc = $('.portfolio-popup-description');
    $portfolioDesc.each(function () {
        new SimpleBar($(this)[0]);
    });

    let $portfolioHeading = $('.portfolio-item-info h3');
    $portfolioHeading.each(function () {
        new SimpleBar($(this)[0]);
    });

    let $expertMessage = $('#expert-slider .expert-message p');
    $expertMessage.each(function () {
        new SimpleBar($(this)[0]);
    });

    let $postDescription = $(".post-feed .row__item .item__description");
    $postDescription.each(function() {
        new SimpleBar($(this)[0])
    })

    // Team carousel

    $('#team .owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        dots: false,
        responsive:{
            0:{
                items:1
            },
            450:{
                items:2
            },
            700:{
                items:3
            },
            950:{
                items:4
            },
            1200:{
                items:5
            }
        }
    });

    $('#partners-slider.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        dots: false,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:3
            },
        }
    });

    $('#expert-slider.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        dots: false,
        responsive:{
            0:{
                items:1
            }
        }
    });

    //  Portfolio get more
    $('#portfolio-get-more').click(function() {
        $.ajax({
            url: window.wp_data.ajax_url,
            data: { action : 'portfolio'},
            type: 'POST',
            success: function(data) {
                $('#portfolio-get-more').remove();
                $('.portfolio-content').append(data);
            }
        });

        $.ajax({
            url: window.wp_data.ajax_url,
            data: { action : 'popup_portfolio'},
            type: 'POST',
            success: function(data) {
                $('.portfolio-popup').append(data);

                $portfolioInfoText = $('.portfolio-item-info .small-text');
                $portfolioInfoText.each(function () {
                    new SimpleBar($(this)[0]);
                });

                $portfolioDesc = $('.portfolio-popup-description');
                $portfolioDesc.each(function () {
                    new SimpleBar($(this)[0]);
                });

                $portfolioHeading = $('.portfolio-item-info h3');
                $portfolioHeading.each(function () {
                    new SimpleBar($(this)[0]);
                });
            }
        });

    });

});